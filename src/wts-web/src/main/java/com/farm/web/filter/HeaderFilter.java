package com.farm.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 头部过滤器
 * @author
 */
public class HeaderFilter implements Filter{

    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpResp = (HttpServletResponse) response;
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        httpResp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS");
        httpResp.setHeader("Access-Control-Allow-Origin", "*");
        httpResp.setHeader("Access-Control-Allow-Credentials","true");
        httpResp.setHeader("Access-Control-Max-Age", "3600");
        if (httpServletRequest.getMethod().equalsIgnoreCase("OPTIONS")) {
            httpResp.setHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,accept,Origin," +
                    "Access-Control-Request-Method,Access-Control-Request-Headers,userID,Authentication");
        }

        chain.doFilter(request, response);
    }


    public void init(FilterConfig arg0) throws ServletException {

    }
}