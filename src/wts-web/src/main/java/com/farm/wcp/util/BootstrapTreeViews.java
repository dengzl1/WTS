package com.farm.wcp.util;

import com.wts.exam.domain.Room;
import org.apache.log4j.Logger;

import java.util.*;

public class BootstrapTreeViews {
	private static final Logger log = Logger.getLogger(BootstrapTreeViews.class);
	/**
	 * 构造树控件的数据对象
	 * 
	 * @param types
	 * @return
	 */
	public static List<Map<String, Object>>  initData(List<Room> types) {

		List<Map<String, Object>> treeData = new ArrayList<>();
		Map<String, Map<String, Object>> treeDataMap = new HashMap<>();
		for (Room type : types) {
			Map<String, Object> node = null;
			// 构造当前节点
			if (treeDataMap.get(type.getId()) == null) {
				node = new HashMap<>();
				node.put("text", type.getName());
				node.put("id", type.getId());
				treeDataMap.put(type.getId(), node);
			} else {
				node = (Map<String, Object>) treeDataMap.get(type.getId());
			}
			treeData.add(node);

		}
		return treeData;
	}
}
